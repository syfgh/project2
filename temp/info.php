<!-- This page is the content page -->
<!DOCTYPE html>
<html lang="en" >

<head>
   <title> Dzungarian Gate</title>
   <meta charset="utf-8">
    <link rel="stylesheet/less" type="text/css" href="../css/style.less">
      <meta name="java" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.3/less.min.js"></script>
    <script src="../sup/project.js" defer></script>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>
<body>

<h1 class="title"></h1>

<nav>
<?php
include "../sup/nav.php";
?>
</nav>

<div class="pic">
<img src="../pic/pic2.jpg" alt="me" height="600" width="800">
</div>

<div class="doc">
 <p>The Dzungarian Gate is a geographically and historically significant mountain pass between China and Central Asia. It has been described as the "one and only gateway in the mountain-wall which stretches from Manchuria to Afghanistan, over a distance of three thousand miles." Given its association with details in a story related by Herodotus, it has been linked to the location of legendary Hyperborea.
<br>
The Dzungarian Gate  is a straight valley which penetrates the Dzungarian Alatau mountain range along the border between Kazakhstan and the Xinjiang Uyghur Autonomous Region.It currently serves as a railway corridor between China and the west. Historically, it has been noted as a convenient pass suitable for riders on horseback between the western Eurasian steppe and lands further east, and for its fierce and almost constant winds.
<br>
In his Histories, Herodotus relates travelers' reports of a land in the northeast where griffins guard gold and where the North Wind issues from a mountain cave. Given the parallels between Herodotus' story and modern reports, scholars such as Carl Ruck, J.D.P. Bolton and Ildik Lehtinen have speculated on a connection between the Dzungarian Gate and the home of Boreas, the North Wind of Greek mythology. With legend describing the people who live on the other side of this home of the North Wind as a peaceful, civilized people who eat grain and live by the sea, the Hyperboreans have been identified by some as the Chinese.
<br>
Its gateway status is now supplanted by the new gateway city of Khorgas.</p>
</div>
<footer>
<?php
include "../sup/footer.php";
?>
</footer>
</body>
</html>