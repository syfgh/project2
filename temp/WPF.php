<!-- This page is the content page -->
<!DOCTYPE html>
<html lang="en" >

<head>
   <title> Dzungarian Gate</title>
   <meta charset="utf-8">
   <link rel="stylesheet/less" type="text/css" href="../css/style.less">
      <meta name="java" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.3/less.min.js"></script>
    <script src="../sup/project.js" defer></script>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>
<body>

<h1 class="title"></h1>

<nav>
<?php
include "../sup/nav.php";
?>
</nav>

<div class="pic">
<img src="../pic/pic6.jpg" alt="me" height="600" width="800">
</div>

<div class="doc wind">

 <p>Wind power supplies 80% electric power in city</p>
</div>
<footer>
<?php
include "../sup/footer.php";
?>
</footer>
</body>
</html>