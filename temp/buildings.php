<!-- This page is the content page -->


<!DOCTYPE html>
<html lang="en" >

<head>
   <title> Dzungarian Gate</title>
   <meta charset="utf-8">
   <link rel="stylesheet/less" type="text/css" href="../css/style.less">
      <meta name="java" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.3/less.min.js"></script>
    <script src="../sup/project.js" defer></script>


<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
</head>
<body>

<h1 class="title"> </h1>

<nav>
<?php
include "../sup/nav.php";
?>
</nav>

<div class="pic">
<img src="../pic/pic3.jpg" alt="me" height="600" width="800">
</div>

<div class="b doc">
The Chinese city of Alashankou lies on the eastern end of the valley in the Bortala Mongol Autonomous Prefecture of Xinjiang. To the west, in the Almaty Province of Kazakhstan, lies its smaller counterpart, Dostyk, or Druzhba in Russian.

Modern development of the pass for its economic potential was delayed by political considerations. An agreement between the Soviet Union and the People's Republic of China to connect Kazakhstan with Western China by rail had been reached in 1954. On the Soviet side, the railway reached the border town of Druzhba (Dostyk) (whose names, both Russian and Kazakh, mean 'friendship') in 1959. On the Chinese side, however, the westward construction of the Lanzhou-Xinjiang railway was stopped once it reached Urumqi in 1962. Due to the Sino-Soviet Split, the border town remained a sleepy backwater for some 30 years, until the Alashankou railway station was finally completed on September 12, 1990.

</div>
<footer>
<?php
include "../sup/footer.php";
?>
</footer>
</body>
</html>