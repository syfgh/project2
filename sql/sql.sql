--create data base;

CREATE DATABASE GATE;
--create table;
create table USERS (
  id varchar(50),
  pw varchar(50)
);

--create action, op1 is test the login or not , op2 is for register;

DELIMITER //
CREATE PROCEDURE op1(ids varchar(50), pws varchar(50))
BEGIN
  SELECT id
  From  users
  Where id=ids and pw=pws;
END //
DELIMITER ;


DELIMITER //
CREATE PROCEDURE op2(id varchar(50), pw varchar(50))
BEGIN

Insert into users values(id,pw);	
END //
DELIMITER ;
--create data for test;
Insert into users values(123,123);

Insert into users values(23,45);

Insert into users values(456,1231);

Insert into users values('test','test');